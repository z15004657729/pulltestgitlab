//
//  PullTestGitLab1App.swift
//  PullTestGitLab1
//
//  Created by zhangguoliang on 2021/11/10.
//

import SwiftUI

@main
struct PullTestGitLab1App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
